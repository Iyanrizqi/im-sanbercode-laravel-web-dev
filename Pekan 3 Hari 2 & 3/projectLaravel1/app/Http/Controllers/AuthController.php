<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis() {
        return view ('halaman.register');
    }
    public function submit(Request $request){
        $firstName = $request ['firstName'];
        $lastName = $request ['lastName'];
        return view ('halaman.welcome', ['firstName' => $firstName, 'lastName' => $lastName]);

    }
}
