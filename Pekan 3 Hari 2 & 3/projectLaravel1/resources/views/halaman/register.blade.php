<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/submit" method = "post">
        @csrf
        <label>First Name :</label> <br>
        <input type="text" name = "firstName"> <br> <br>
        <label>Last Name :</label> <br>
        <input type="text" name = "lastName"> <br> <br>
        <label>Gender :</label> <br>
        <input type="radio" name = "gender" value = "1">Man <br>
        <input type="radio" name = "gender" value = "2">Woman <br>
        <input type="radio" name = "gender" value = "3">Other <br> <br>
        <label>Nationality :</label> <br>
        <select name="nationality">
            <option value="">Indonesian</option>
            <option value="">American</option>
            <option value="">British</option>
        </select> <br> <br>
        <label>Language Spoken :</label ><br>
        <input type="checkbox" name = "bahasa">Bahasa Indonesia <br>
        <input type="checkbox" name = "bahasa">English <br>
        <input type="checkbox" name = "bahasa">Arabic <br>
        <input type="checkbox" name = "bahasa">Japanese <br> <br>
        <label>Bio :</label> <br>
        <textarea name="" id="" cols="30" rows="10"></textarea> <br>
        <input type="submit" value = "Sign Up">
    </form>

</body>
</html>